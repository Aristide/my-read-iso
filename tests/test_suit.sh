#!/bin/sh

#./$1 tests/myiso.iso < tests/list_command > my_Output.txt
#./$1
#echo $?
#./$1 strangeArgument
#echo $?

#diff --color=auto -s tests/OutputEpita.txt my_Output.txt
echo "*****************PROMPT TESTS*******************"
./$1 tests/myiso.iso
echo "************GENERAL FEATURES************"

./$1 tests/myiso.iso < tests/list_command
./$1 tests/myiso.iso < tests/list_command > my_Output.txt
./epita tests/myiso.iso < tests/list_command > OutputEpita.txt

echo "***********INFO TESTS*******************"

echo info | ./$1 tests/myiso.iso
echo info | ./$1 tests/myiso.iso > my_Output.txt
echo info | ./epita tests/myiso.iso > OutputEpita.txt

echo "***********LS TESTS********************"
echo ls | ./$1 tests/myiso.iso
echo ls | ./$1 tests/myiso.iso > my_Output.txt
echo ls | ./epita tests/myiso.iso > OutputEpita.txt

echo "*********CAT TESTS*********************"
echo 'cat README.TXT' | ./$1 tests/example.iso
echo 'cat README.TXT' | ./$1 tests/example.iso > my_Output.txt
echo 'cat README.TXT' | ./epita tests/example.iso > OutputEpita.txt

echo "*********HELP TESTS********************"
echo help | ./$1 tests/example.iso
echo help | ./$1 tests/example.iso > my_Output.txt
echo help | ./epita tests/example.iso > OutputEpita.txt

echo "*********CD TESTS**********************"
./$1 tests/myiso.iso < tests/cd_cmd
./$1 tests/myiso.iso < tests/cd_cmd > my_Output.txt
./epita tests/myiso.iso < tests/cd_cmd > OutputEpita.txt

echo "********GET TESTS**********************"
cat README.TXT
echo 'get README.TXT' | ./$1 tests/example.iso
cat README.TXT

echo "*******TREE TESTS**********************"
echo 'tree' | ./$1 tests/myiso.iso
echo 'tree' | ./$1 tests/myiso.iso > my_Output.txt
echo 'tree' | ./epita tests/myiso.iso > OutputEpita.txt
