CC=gcc
CFLAGS= -Wall -Wextra -Werror -pedantic -std=c99 -I./include -g
SRC=src/my_read_iso.c src/command.c src/auxil.c src/auxil2.c src/auxil3.c

BIN=my_read_iso 

OBJETS=my_read_iso.o

VPATH=src/ 


.PHONY: all

all:$(OBJETS)
	$(CC) $(CFLAGS) $(SRC) -o $(BIN)

$(BIN)=$(OBJETS)

.PHONY: check
test:$(BIN)
	./tests/test_suit.sh $(BIN)

.PHONY: clean
clean:
	${RM} *.o *.swp *.txt ${BIN}
