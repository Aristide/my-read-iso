#ifndef AUXIL_H
#define AUXIL_H
#include "command.h"
#define OK 0
#define BAD 1

/*useful in all commands*/
void *voidance(void *p);
void *towards(void* p, int len);
char *handle_space(char *s1, char *s2);

/*Useful in ls_command*/
void cur_affect(char *s1, char tab[], char *s1_val, char tab0_val, char
tab1_val);
void par_affect(char *s1, char tab[], char *s1_val, char tab0_val, char
tab1_val,char tab2_val);
void iso_func(char *s1, struct iso_dir *iso_d, int val, char tab1[]);
void check_dir_type(struct iso_dir *iso_d, char isDir_, char isHidden_);
/*Useful in cat_command and get command*/
void transfert(char *tab1, char *tab2);

/*Useful in cd command*/
void cd_transfert(char *tab1, char *tab2);
void cd_handle_root(char *s, char *tab1, char *tab2, int val, struct iso_dir *iso_d);
void cd_empty_entry(char *tab1, char *tab2, int val, struct iso_dir *iso_d);
void cd_dash_entry(char *s, int val, char *tab2, struct iso_dir *iso_d);
int check_entry_type(struct iso_dir *iso_d, char *s);
void cd_handle_change(char *s, char *s1, char *s2, char *s3, int val, char *s5, char *s6);
/*Useful in tree command*/
int check_depth(int d);
void tree_transf(char *s1, char *s2, int val);
void contains(int tab[64], int val);
void tree_tempo(struct iso_dir *iso_t , char *file, int tab[64], int val);
void tree_func(struct iso_dir *iso_t, int tab[64], int val1, int val2);
void tr_info(int val1, int val2);
/**/






#endif
