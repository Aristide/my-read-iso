#ifndef COMMAND_H
#define COMMAND_H
#include"iso9660.h"
#include<stdio.h>    
#include<string.h>    
#include<stddef.h>    
#include <fcntl.h>
#define SYS_PRE_SIZE 32768

/*global for cd_command*/
char *g_current_d;
char *g_actual_d;
char *g_parrent_d;
char *g_previous_d;
int g_previous_b;
int g_root_blk;
/*global for tree_command*/
int nbfile;
int nbdir;
int contain[64];

void help_command(void);
void info_command(struct iso_prim_voldesc *iso_pvd);
void ls_command(char *file);
void cat_command(char *iso_pv, char *cur_dir, char *name);
void get_command(char *iso_pv, char *curren_dir, char *name);
char *cd_command(char *iso_pv, char *actual_d, char *name, char **current_d, char
**parrent_d, char **previous_d, int *previous_b, int rootb);
void tree_command(int depth, char *curren_d, int *nbdir, int *nbfile, int
has_file[64]);
void init_dir(char *current, char *parrent, char *previous_d);
void init_tab(int tab[64]);
void init_tree(int file_, int dir_, int contain_[64]);


#endif
