#include<stdio.h>                                                               
#include<string.h>                                                              
#include<stddef.h>                                                              
#include "include/command.h"
#include "include/auxil.h"
#include <fcntl.h>

/**********************HELP******************************/
void help_command(void)
{
  printf("help: display command help\n");
  printf("info: display volume info\n");
  printf("ls: display the content of a directory\n");
  printf("cd: change current directory\n");
  printf("tree: display the tree of a directory\n");
  printf("get: copy file to local directory\n");
  printf("cat: display file content\n");
  printf("pwd: print current path\n");
  printf("quit: program exit\n");
}
/****************************INFO******************************/
void info_command(struct iso_prim_voldesc *iso_pvd)
{
  printf("System Identifier: %.*s\n",ISO_SYSIDF_LEN,iso_pvd->syidf);          
  printf("Volume Identifier: %.*s\n",ISO_VOLIDF_LEN,iso_pvd->vol_idf);         
  printf("Block count: %d\n",iso_pvd->vol_blk_count.le);
  printf("Block size: %d\n",iso_pvd->vol_blk_size.le);
  printf("Creation date: %.*s\n",ISO_LDATE_LEN, iso_pvd->date_creat);         
  printf("Application Identifier: %.*s\n",ISO_APP_LEN, iso_pvd->app_idf);     
}
/***********************LS*****************************/
void ls_command(char *file)
{
  char isDir = '-';
  char isHidden = '-';
  char *cur_name = NULL;
  char file_name[100] = {0};
  struct iso_dir *s_iso_dir = voidance(file);
  
  for (int i = 0; s_iso_dir->idf_len != 0 ; i++)
  {
    if (i == 0)
      cur_affect(cur_name, file_name, ".", '.', '\0');
    else if (i == 1)
      par_affect(cur_name, file_name, ".", '.', '.','\0');
    else
    {
      int len = s_iso_dir->idf_len;
      iso_func(cur_name, s_iso_dir, len, file_name);
    }
    check_dir_type(s_iso_dir, isDir, isHidden);
    char *iso_date = s_iso_dir->date;
    int yea = 1900 + iso_date[0];
    int mon = iso_date[1];
    int day = iso_date[2];
    int hou = iso_date[3];
    int min = iso_date[4];
    uint32_t size = s_iso_dir->file_size.le;
    printf("%c%c %9u %04d/%02d/%02d %02d:%02d %s\n", isDir, isHidden, size,
        yea, mon, day, hou, min, file_name);
    int next = sizeof(struct iso_dir) + s_iso_dir->idf_len;
    if (s_iso_dir->idf_len % 2 == 0)
      next++;
    s_iso_dir = towards(s_iso_dir, next);
  }
}

/*******************CAT**************************/
void cat_command(char *iso_pv, char *cur_dir, char *name)
{
  char name_f[1024] = {0};
  transfert(name_f,name);
  struct iso_dir *s_iso_dir = towards(cur_dir, 68);
  for (int i = 0; s_iso_dir->idf_len != 0; i++)
  {
    char *name_s = (char*)s_iso_dir + sizeof(struct iso_dir);
    int len = s_iso_dir->idf_len;
    
    if (s_iso_dir->type != 2)
      len -=2;
    if (strncmp(name_s, name_f, len) == 0)
    {
      if (s_iso_dir->type == 2 || s_iso_dir->type == 3)
      {
        fprintf(stderr, "entry '%s' is a directory\n", name_f);
        return;
      }
      char *c_file = iso_pv + s_iso_dir->data_blk.le * ISO_BLOCK_SIZE - SYS_PRE_SIZE;
      fwrite(c_file, sizeof(c_file),s_iso_dir->file_size.le, stdout);
      return;
    }
    int next = sizeof(struct iso_dir) + s_iso_dir->idf_len;
    if (s_iso_dir->idf_len % 2 == 0)
      next++;
    s_iso_dir = towards(s_iso_dir, next);
  }  
  fprintf(stderr,"unable to find '%s' entry\n", name_f);
}

/**************************GET_COMMAND**************************/
void get_command(char *iso_pv, char *curren_dir, char *name)
{
  char name_f[1024] = {0};
  transfert(name_f, name);
  struct iso_dir *s_iso_dir = towards(curren_dir, 68);
  for (int i = 0; s_iso_dir->idf_len != 0; i++)
  {
    char *name_s = (char*)s_iso_dir + sizeof(struct iso_dir);                      
    int len = s_iso_dir->idf_len;                                      
    
    if (s_iso_dir->type != 2) 
      len -= 2;                            
    if (strncmp(name_s, name_f, len) == 0)
    {                                  
      if (s_iso_dir->type == 2 || s_iso_dir->type == 3) 
      {
        fprintf(stderr, "entry '%s' is a directory\n", name_f);
        return; 
      } 
      char *g_file = iso_pv + s_iso_dir->data_blk.le * ISO_BLOCK_SIZE - SYS_PRE_SIZE;
      FILE *n_file = fopen(name_f, "w+");                 
      fwrite(g_file, sizeof(g_file), s_iso_dir->file_size.le, n_file);
      fclose(n_file);                                                          
      return; 
    }
    int next = sizeof (struct iso_dir) + s_iso_dir->idf_len;
    if (s_iso_dir->idf_len % 2 == 0)
      next++;
    s_iso_dir = towards(s_iso_dir,next);
  }
  fprintf(stderr, "unable to find '%s' entry\n", name_f);
  
}

/************************CD_COMMAND*******************************/
char *cd_command(char *iso_pv, char *curren_d, char *name, char **current, char **parrent, char **previous_d, int *previous_b, int rootb)
{
   char name_d[1024] = {0};
   cd_transfert(name_d, name);
   struct iso_dir *s_iso_dir = voidance(curren_d);

   if (strcmp(name_d, ".") == 0)
   {
    printf("Changing to '%s' directory\n", curren_d);
    return curren_d;
   }
   
   if (strcmp(name_d, "..") == 0)
   {
    struct iso_dir *s_iso_dir = towards(curren_d, 34);
    char *to_dir = (char*)iso_pv + s_iso_dir->data_blk.le * ISO_BLOCK_SIZE - SYS_PRE_SIZE;
        
    cd_handle_root(*parrent, *previous_d, *current, *previous_b, s_iso_dir);
    return to_dir;       

   }
   if (strlen(name_d) == 0)
   {
     char *to_dir = (char*)iso_pv + rootb  * ISO_BLOCK_SIZE - SYS_PRE_SIZE;
     cd_empty_entry(*previous_d, *current, *previous_b, s_iso_dir);
     return to_dir;
   }
    
   if (strcmp(name_d,"-") == 0)
   {
    char *to_dir = (char*)iso_pv + *previous_b  * ISO_BLOCK_SIZE - SYS_PRE_SIZE;
    cd_dash_entry(*previous_d, *previous_b, *current, s_iso_dir);
    return to_dir;
   }
   
   s_iso_dir = voidance(curren_d);
   for (int i = 0; s_iso_dir->idf_len != 0; i++)
   {
    char *name_c = towards(s_iso_dir, sizeof (struct iso_dir));
    int len = s_iso_dir->idf_len;
        
    if (s_iso_dir->type != 2)
      len -= 2;
    if (strncmp(name_c, name_d, len) == 0)
    {   
      if (check_entry_type(s_iso_dir, name_d) == 0)
        return curren_d;
      char *to_dir = (char*)iso_pv + s_iso_dir->data_blk.le * ISO_BLOCK_SIZE - SYS_PRE_SIZE;
      cd_handle_change(name_d, *previous_d, *current, curren_d, *previous_b, *parrent, name);
      return to_dir;
    }

    int next = sizeof (struct iso_dir) + s_iso_dir->idf_len;
    
    if (s_iso_dir->idf_len % 2 == 0)
      next++;
    s_iso_dir = towards(s_iso_dir, next);
    
   }
 
    printf("unable to find '%s' directory entry\n", name_d);
    
    return curren_d;
}

void tree_command(int depth, char *curren_d, int *nbdir, int *nbfile, int 
contain_f[1024])
{  
  if (check_depth(depth) == OK)
    depth += 1;
  if (depth > 0)
    {
      struct iso_dir *s_iso_dir = voidance(curren_d + 68);
      int len = s_iso_dir->idf_len;
      char *name = (char*)s_iso_dir + sizeof (struct iso_dir);
      char name_f[1024] = {0};
      
      for (int i = 0; s_iso_dir->idf_len != 0; i++)
      {
        if (s_iso_dir->type != 2 && s_iso_dir->type != 3)
          len -= 2;
        tree_transf(name_f, name,len);
        contains(contain_f, depth);
        int next = sizeof (struct iso_dir) + s_iso_dir->idf_len;
        
        if (s_iso_dir->idf_len % 2 == 0)
          next++;
        
        struct iso_dir *iso_t = towards(s_iso_dir, next);
        tree_tempo(iso_t, name_f, contain_f, depth);
    
        if (s_iso_dir->type == 2 || s_iso_dir->type == 3)
        {
          tree_func(iso_t, contain_f, depth, *nbdir);
          (*nbdir)++;
          struct iso_dir *s_iso_dir2 = voidance(curren_d);
          char *nextdir = curren_d + (s_iso_dir->data_blk.le - s_iso_dir2->data_blk.le) *
          ISO_BLOCK_SIZE;
          tree_command(depth + 1, nextdir, nbdir, nbfile, contain_f);
        }
        else
        {
          (*nbfile)++;
          printf("\n");
        }
        if (depth == 1 && iso_t->idf_len == 0)
          tr_info(*nbdir, *nbfile);
        s_iso_dir = towards(s_iso_dir, next);
    }
  }
}
