#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/mman.h>
#include<string.h>
#include<err.h>
#include<errno.h>
#include<stdlib.h>
#include "include/iso9660.h"
#include "include/command.h"
#include "include/auxil.h"


int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        fprintf(stderr,"usage: ./my_read_iso FILE\n");
        return 1; 
    } 
    
    int fildes=0;
    if ((fildes = open(argv[1],O_RDONLY)) == -1)
        err(1, "%s", argv[1]);

    struct stat g_buf;
    if (fstat(fildes,&g_buf) == -1)
        errx(1,"%s",strerror(errno)); 
    
    /*SANITY CHECK*/ 
    unsigned int size_convert = g_buf.st_size;
    if (size_convert < ((ISO_PRIM_VOLDESC_BLOCK * ISO_BLOCK_SIZE) + sizeof(struct iso_prim_voldesc)))
        {
            errx(1,"%s: invalid ISO9660 file",argv[1]); 
        }
    struct stat fildes_buf;
    fstat (fildes, &fildes_buf);

    struct iso_prim_voldesc *iso_pvd = mmap(NULL, g_buf.st_size, 
    PROT_READ,MAP_PRIVATE, fildes, 
   ISO_PRIM_VOLDESC_BLOCK * ISO_BLOCK_SIZE);
   
   struct iso_dir root_info = iso_pvd->root_dir;
   char *root_dir = (char*)iso_pvd + root_info.data_blk.le * ISO_BLOCK_SIZE - SYS_PRE_SIZE;
   g_actual_d = root_dir;
   init_dir(g_current_d, g_parrent_d, g_previous_d);
   g_previous_b = root_info.data_blk.le;
   g_root_blk = root_info.data_blk.le;     
   
    if (iso_pvd  == MAP_FAILED)
        errx(1, "%s",strerror(errno));
    if (strncmp(iso_pvd->std_identifier,"CD001",5))
        errx(1, "%s: invalid ISO9660 file",argv[1]);

    char buffer[4095] = {0};
    char cmd[4095] = {0};
    char cmd_edit[6000] = {0};
    int j = 0;
    
    while (1)
    {
        if (isatty(0))
            fwrite("> ",sizeof("> "),1,stdout);
        if (fgets(buffer,4095,stdin) == NULL)
            return 1;
        int i=0;
        j=0;
        for (;buffer[j] != '\0' && buffer[j]!='\n'; i++)
        {
            cmd[i] = buffer[j];
            j++;
        }
        j++;
        cmd[i] = '\0';
        
        handle_space(cmd,cmd_edit);
        if (isatty(0)) 
            j = 0;   

        if (strcmp(cmd_edit,"help") == 0)
            help_command();
        else if (strncmp(cmd, "help ",5) == 0)
            fprintf(stderr,"my_read_iso: help: command does not take an argument\n");
        else if (strcmp(cmd_edit, "info") == 0)
            info_command(iso_pvd);
        else if (strncmp(cmd, "info ", 5) == 0)
            fprintf(stderr, "%s: info: command does not take an argument\n",argv[0]);
        
        else if (strcmp(cmd_edit, "ls") == 0)
          ls_command(g_actual_d);
        else if(strncmp(cmd, "ls ", 3) == 0)
          fprintf(stderr, "my_read_iso: ls: command does not take an argument\n");
        else if (strncmp(cmd, "cat ", 4) == 0)
          cat_command((char*)iso_pvd, g_actual_d, cmd);

        else if (strncmp(cmd, "get ", 4) == 0)
          get_command((char*)iso_pvd, g_actual_d, cmd);

        else if (strncmp(cmd, "cd ", 3) == 0 || strncmp(cmd, "cd", 2) == 0)
          g_actual_d = cd_command((char*)iso_pvd, g_actual_d, cmd, &g_current_d, 
              &g_parrent_d, &g_previous_d, &g_previous_b, g_root_blk);

        else if (strcmp(cmd_edit, "tree") == 0)
        {
          init_tree(nbdir, nbfile, contain); 
          tree_command(0, g_actual_d, &nbdir, &nbfile, contain);
          init_tab(contain);
        }
      
        else if (strncmp(cmd, "tree ", 5) == 0)
          fprintf(stderr, "my_read_iso: tree: command does not take an argument\n");

        else if (strcmp(cmd_edit, "quit") == 0)
          exit(0);

        else if (strncmp(cmd,"quit ",5) == 0)
          fprintf(stderr, "%s: quit: command does not take an argument\n",argv[1]);
            
        else if (! *cmd_edit)
        {
            if (!isatty(0))
                exit(0);
            continue;
        }
    
        else
            fprintf(stderr, "%s: %s: unknown command\n", argv[0],cmd_edit);
        
        for (int k = 0; k < 4095; k++)
            cmd_edit[k] = '\0';

}    
    return 0;

}


