#include "include/auxil.h"

void cd_handle_root(char *s, char *tab1, char *tab2, int val, struct iso_dir *iso_d)
{
  printf("Changing to '%s' directory\n",s);
  tab1 = tab2;
  s = "/";
  val = iso_d->data_blk.le;
  tab2 = s;
  (void)tab1;
  (void)val;

}
void cd_empty_entry(char *tab1, char *tab2, int val, struct iso_dir *iso_d)
{
  printf("Changing to '/' directory\n");
  tab1 = tab2;
  val = iso_d->data_blk.le;
  tab2 = "/";

  (void)tab1;
  (void)val;
}
void cd_dash_entry(char *s, int val, char *tab2, struct iso_dir *iso_d)
{
  printf("Changing to '%s' directory\n", s);
  char *t = s;
  val = iso_d->data_blk.le;
  s = tab2;
  tab2 = t;

  (void)val;
}
int check_entry_type(struct iso_dir *iso_d, char *s)
{
  if (iso_d->type != 2 && iso_d->type != 3)
  {
    fprintf(stderr, "entry '%s' is not a directory\n", s);
    return 0;
  }
  return 1;
}
void cd_handle_change(char *s, char *s1, char *s2, char *s3, int val, char *s5, char *s6)
{
  printf("Changing to '%s' directory\n", s);
  s1 = s2;
  struct iso_dir *new_sid = voidance(s3);
  val = new_sid->data_blk.le;
  s5 = s2;
  s2 = s6;

  (void)val;
  (void)s1;
  (void)s5;
}
