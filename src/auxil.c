#include "include/auxil.h"

void *voidance(void *p)
{
  return p;
}

void *towards(void* p, int len)
{
  char *s = p;
  return s + len;
}

char *handle_space(char *s1, char *s2)
{                        
  size_t nb = strlen(s1);
  int j = 0;   
  size_t i = 0;
                 
  while (i < nb )
  {                                       
    if ((s1[i] != ' ') && (s1[i] != '\t'))
    {               
      s2[j] = s1[i];
      i++;
      j++;
    }
    
    else  
      i++;
   }          
    return s2;
}

void cur_affect(char *s1, char tab[], char *s1_val, char tab0_val, char
tab1_val)
{ 
  s1 = s1_val;
  tab[0] = tab0_val;
  tab[1] = tab1_val;
  
  (void)s1;
}


void par_affect(char *s1, char tab[], char *s1_val, char tab0_val, char
tab1_val,char tab2_val)
{             
  s1 = s1_val;      
  tab[0] = tab0_val;
  tab[1] = tab1_val;
  tab[2] = tab2_val;
  
  (void)s1;
}


void iso_func(char *s1, struct iso_dir *iso_d, int val, char tab1[])
{
  s1 = (char*)iso_d + sizeof(struct iso_dir);

  if (iso_d->type != 2 && iso_d->type != 3)
    val -= 2;
  for (int j = 0; j < val ; j++)
    tab1[j] = s1[j];
}

void check_dir_type(struct iso_dir *iso_d, char isDir_, char isHidden_)
{
  if (iso_d->type == 2)
    isDir_ = 'd';
  if (iso_d->type == 1)
     isHidden_ = 'h';
  if (iso_d->type == 3)
  {
    isDir_ = 'd';
    isHidden_ = 'h';
  }

  (void)isDir_;
  (void)isHidden_;
}

void transfert(char *tab1, char *tab2)
{
  int i = 0;
  for (; tab2[i] != '\0'; i++)
  {
    if (tab2[i] == ' ' && tab2[i + 1] != ' ')
      break;
  }
  i++;
  int j = 0;
  for (; tab2[i + j] != '\0'; j++)
    tab1[j] = tab2[i + j]; 
}

void cd_transfert(char *tab1, char *tab2)
{
  int i = 0;
  for (; tab2[i] != '\0'; i++)
  {
    if (tab2[i] == ' ' && tab2[i + 1] != ' ')
      break;
  }
  if (i != 3)
    i++;
  int j = 0;
  for (; tab2[i + j] != '\0'; j++)
    tab1[j] = tab2[i + j];
  tab2[j] = '\0'; 
}
