#include "include/auxil.h"

int check_depth(int d)
  {
      if (d == 0)
      {
        printf(".\n");
         return OK;
      }
     return BAD;
  }


void tree_transf(char *s1, char *s2, int val)
{
  for (int j = 0; j < val; j++)
    s1[j] = s2[j];
}

void contains(int tab[64], int val)
{
  for (int i = 0; i < val - 1; i++)
  {
    if (tab[i])
      printf("|");
    else
    {
      printf(" ");
      printf("   ");
     }
   }
}

void tree_tempo(struct iso_dir *iso_t , char *file, int tab[64], int val)
{
  if (iso_t->idf_len != 0)
    printf("|-- %s", file);
  else
  {
    printf("+-- %s", file);
    tab[val - 1] = 0;
  }
}

void tree_func(struct iso_dir *iso_t, int tab[64], int val1, int val2)
{
  if (iso_t->idf_len != 0)
    tab[val1 - 1] = 1;
  (val2)++;
  printf("/\n");
}


void tr_info(int val1, int val2)
{
  printf("\n");
  printf("%d directories, %d files\n", val1, val2);
}

void init_dir(char *current, char *parrent, char *previous_d)
{
  current = "/";
  parrent = "/";
  previous_d = "/";

  (void)current;
  (void)parrent;
  (void)previous_d;
}

void init_tab(int tab[64])
{
  for (int k = 0; k < 64; k++)
    tab[k] = 0;
}


void init_tree(int file_, int dir_, int contain_[64])
{
  file_ = 0;
  dir_ = 0;
  init_tab(contain_);

  (void)file_;
  (void)dir_;
}
